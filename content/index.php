<?php include("../main/head.php");?>


<?php include("../main/header.php");?>

<?php include("../main/menu.php");?>  

 
	
	<div id="bloque_contenido" class="span-24" >

		<span> Prisma es una empresa que nace para ser la solución a todas las necesidades de oficina. Nosotros somos importadores y		   
				distribuidores de papelería, limpieza, cartuchos, toners, cafetería, y electrónicos. Contamos con más de mil diferentes productos  
				en inventario de manera permanente, comercializando así las marcas más conocidoas del mercado. </span>
				<br>
				</br>
	      <span>Nuestro servicio es uno de nuestros mejores activos, ya que nuestro equipo se conforma por gente altamente capacitada en el área para satisfacerlo. Contamos con equipo de ventas que conoce cada producto y lo puede 
				guiar 
				en sus compras. Un departamente de despacho y almacenamiento que mantiene los productos en el mejor estado y realiza las entregas en menos de quince horas laborables. Y un departamento de gerencia que día a día 
				trabaja 	
				para mantener los mejores precios y ofertar sólo productos de alta calidad.</span>
				<br>
				</br>
	      <span>Llámanos para que uno de nuestros ejecutivos de ventas te asista en todas tus necesidades de oficina.</span>
				<br>
				</br>
	      <span id="telefono">Tel: (809) 563-1114 </span>
				<br>
	      <span id="fax">Fax: (809) 563-0798</span>
				<br>
	      <span >Santo Domingo, República Dominicana</span>
  
	</div>
	
<?php include("../main/footer.php");?>  