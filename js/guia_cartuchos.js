require(["dojox/grid/DataGrid" /*, other deps */, "dojo/domReady!"],
    function(DataGrid /*, other deps */){
        /* create store here ... */
         
        var grid = new DataGrid({
            store: store,
            query: { id: "*" },
            structure: [
                { name: "marca", field: "Marca", width: "84px" },
                { name: "tipo", field: "Tipo de Impresora", width: "84px" },
                { name: "modelo", field: "Modelo", width: "70px" },
                { name: "color", field: "Color", width: "70px" },
                { name: "EOM", field: "Parte EOM", width: "60px" },
                { name: "paginas", field: "Paginas", width: "60px" },
                { name: "tech", field: "Techonologia de impresion", width: "120px" }
            ]
        }, "grid");
         
});